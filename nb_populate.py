#!/usr/bin/env python3

import pynetbox
import subprocess
from docker import APIClient
from termcolor import colored

# connect to docker daemon api
dockapi = APIClient()

# get the nginx container
nginx = [container for container in dockapi.containers() if "/netbox-docker_nginx_1" in container['Names']][0]

# get public port and IP from nginx container
for spec in nginx['Ports']:
    if 'PublicPort' in spec.keys():
        nb_port = str(spec['PublicPort'])
        nb_ip = str(spec['IP'])

# connect to netbox
nb = pynetbox.api(f"http://{nb_ip}:{nb_port}/",token="0123456789abcdef0123456789abcdef01234567")

# set some static vars for the sites, racks
sites = ['c99']
racks = ['k01', 'f01', 'f16']

# these are 'friendly' vars for colours and device direction in racks
colours = {'green': '97ebdb', 'blue': '005582', 'pink': 'f9c3d3'}
face = {'front': 0, 'rear': 1}


# some resource creation tasks require IDs instead of names
# these functions simply look up resource IDs and raise errors if the
# resource doesn't exist

def manufacturer_id_lookup(manufacturer_name):
    try:
        return nb.dcim.manufacturers.filter(name=manufacturer_name)[0].id
    except Exception as e:
        print(colored(f"manufacturer {manufacturer_name} does not exist", 'red'))
        raise(e)

def site_id_lookup(site_name):
    try:
        return nb.dcim.sites.filter(name=site_name)[0].id
    except Exception as e:
        print(colored(f"site {site_name} does not exist", 'red'))
        raise(e)

def rack_id_lookup(rack_name, site_name):
    try:
        return nb.dcim.racks.filter(name=rack_name, site=site_name)[0].id
    except Exception as e:
        print(colored(f"rack {rack_name} does not exist in site {site_name}", 'red'))
        raise(e)

def device_type_id_lookup(model):
    try:
        return nb.dcim.device_types.filter(model=model)[0].id
    except Exception as e:
        print(colored(f"device type {model} does not exist", 'red'))
        raise(e)

def device_role_id_lookup(device_role):
    try:
        return nb.dcim.device_roles.filter(name=device_role)[0].id
    except Exception as e:
        print(colored(f"device role {device_role} does not exist", 'red'))
        raise(e)

# these functions instantiate resources in netbox
# some resources have dependent relationships
# for example racks require a site to exist

def make_site(site_name):
    existing_sites = [site.name for site in nb.dcim.sites.all()]
    if site_name not in existing_sites:
        nb.dcim.sites.create({'name': site_name, 'slug': site_name.lower()})
        print(colored(f"created site {site_name}", 'yellow'))
    else:
        print(colored(f"site {site_name} already exists", 'green'))

def make_rack(site_name, rack_name):
    site_id = site_id_lookup(site_name)
    site_racks = [rack.name for rack in nb.dcim.racks.filter(site=site_name)]
    if rack_name not in site_racks:
        nb.dcim.racks.create({'status': 3, 'site': site_id, 'name': rack_name})
        print(colored(f"created rack {rack_name} in site {site_name}", 'yellow'))
    else:
        print(colored(f"rack {rack_name} already exists in site {site_name}", 'green'))

def make_manufacturer(manufacturer_name):
    existing_manufacturers = nb.dcim.manufacturers.all()
    if manufacturer_name not in [manufacturer.name for manufacturer in existing_manufacturers]:
        nb.dcim.manufacturers.create(name=manufacturer_name, slug=manufacturer_name.lower())
        print(colored(f"created manufacturer {manufacturer_name}", 'yellow'))
    else:
        print(colored(f"manufacturer {manufacturer_name} already exists", 'green'))

def make_device_role(device_role_name, device_role_colour):
    if device_role_colour not in colours.keys():
        print(colored(f"invalid colour choice {device_role_colour} please choose from {colours.keys()}", 'red'))
    existing_device_roles = [device_role.name for device_role in nb.dcim.device_roles.all()]
    if device_role_name not in existing_device_roles:
        nb.dcim.device_roles.create({'name': device_role_name,
                                        'slug': device_role_name.lower(), 'color': colours[device_role_colour]})
        print(colored(f"created device role {device_role_name}", 'yellow'))
    else:
        print(colored(f"device role {device_role_name} already exists", 'green'))

def make_device_type(manufacturer, model, height, depth):
    manufacturer_id = manufacturer_id_lookup(manufacturer)
    existing_device_types = nb.dcim.device_types.all()
    if model not in [device_type.model for device_type in existing_device_types]:
        nb.dcim.device_types.create({'manufacturer': manufacturer_id, 'model': model,
                                        'slug': model.lower(), 'height': height, 'depth': depth})
        print(colored(f"created device type {model}", 'yellow'))
    else:
        print(colored(f"device type {model} already exists", 'green'))

def make_device(device_name, device_type, device_role, site_name):
    existing_devices = [device.name for device in nb.dcim.devices.all()]
    site_id = site_id_lookup(site_name)
    device_role_id = device_role_id_lookup(device_role)
    device_type_id = device_type_id_lookup(device_type)
    if device_name not in existing_devices:
        nb.dcim.devices.create({'name': device_name, 'device_type': device_type_id,
                                    'device_role': device_role_id, 'site': site_id})
        print(colored(f"created device {device_name} in site {site_name}", 'yellow'))
    else:
        print(colored(f"device {device_name} already exists", 'green'))

def position_device(device_name, site_name, rack_name, rack_position, rack_face):
    try:
        rack_id = rack_id_lookup(rack_name, site_name)
        device = nb.dcim.devices.filter(name=device_name, site=site_name)[0]
        if device.rack == None or device.face == None:
            device.update({'rack': rack_id, 'position': rack_position, 'face': face[rack_face]})
            print(colored(f"positioned device {device_name} in rack {rack_name}, u{rack_position}", 'yellow'))
        elif device.rack.id != rack_id or device.position != int(rack_position) or device.face.value != face[rack_face]:
            device.update({'rack': rack_id, 'position': rack_position, 'face': face[rack_face]})
            print(colored(f"positioned device {device_name} in rack {rack_name}, u{rack_position}", 'yellow'))
        else:
            print(colored(f"device {device_name} already in rack {rack_name}, u{rack_position}", 'green'))
    except Exception as e:
        print(e)
        print(colored(f"could not locate {device_name} at site {site_name}", 'red'))

def make_ip_address(ip_address, description, dns_name):
    if len(nb.ipam.ip_addresses.filter(address=ip_address)) == 0:
        nb.ipam.ip_addresses.create({'address': ip_address, 'status': 1,
                                        'description': description, 'dns_name': dns_name})
        print(colored(f"created ip {ip_address}", 'yellow'))
    else:
        print(colored(f"ip {ip_address} already exists", 'green'))

# populate sites & racks
for site_name in sites:
    make_site(site_name)
    for rack_name in racks:
        make_rack(site_name, rack_name)

# create manufacturers
make_manufacturer('arista')

# create switch models
make_device_type('arista', 'dcs-7050cx3-48yc8-r', 1, 'full')
make_device_type('arista', 'dcs-7060cx2-32s-r', 1, 'full')

# make device roles
make_device_role('leaf', 'green')
make_device_role('spine', 'pink')

# create devices with minimal data
make_device('test-c99-f0101-sc01', 'dcs-7060cx2-32s-r', 'spine', 'c99')
make_device('test-c99-f0102-ltr01a', 'dcs-7050cx3-48yc8-r', 'leaf', 'c99')
make_device('test-c99-f0103-lyc01a', 'dcs-7050cx3-48yc8-r', 'leaf', 'c99')
make_device('test-c99-f1602-ltr01b', 'dcs-7050cx3-48yc8-r', 'leaf', 'c99')
make_device('test-c99-f1603-lyc01b', 'dcs-7050cx3-48yc8-r', 'leaf', 'c99')
make_device('test-c99-k0101-sc01', 'dcs-7060cx2-32s-r', 'spine', 'c99')

# put switches in racks
position_device('test-c99-f0101-sc01', 'c99', 'f01', '01', 'rear')
position_device('test-c99-f0102-ltr01a', 'c99', 'f01', '02', 'rear')
position_device('test-c99-f0103-lyc01a', 'c99', 'f01', '03', 'rear')
position_device('test-c99-f1602-ltr01b', 'c99', 'f16', '02', 'rear')
position_device('test-c99-f1603-lyc01b', 'c99', 'f16', '03', 'rear')
position_device('test-c99-k0101-sc01', 'c99', 'k01', '01', 'rear')

#create some IPs
make_ip_address("10.168.100.10/32", "test-c99-f0101-sc01 Ma0", "test-c99-f0101-sc01.test.local")
make_ip_address("10.168.100.11/32", "test-c99-k0101-sc01 Ma0", "test-c99-k0101-sc01.test.local")
make_ip_address("10.168.100.12/32", "test-c99-f0102-ltr01a Ma0", "test-c99-f0102-ltr01a.test.local")
make_ip_address("10.168.100.13/32", "test-c99-f0103-lyc01a Ma0", "test-c99-f0103-lyc01a.test.local")
make_ip_address("10.168.100.14/32", "test-c99-f1602-ltr01b Ma0", "test-c99-f1602-ltr01b.test.local")
make_ip_address("10.168.100.15/32", "test-c99-f1603-lyc01b Ma0", "test-c99-f1603-lyc01b.test.local")
