# onug-ipam

IP address management is a necessary part of a network engineer's role.
We've achieved this in a variety of ways over time for the purposes of allocating IP address space in a hopefully non-overlapping fashion.

A common tool is the humble spreadsheet, but there are limitations in spreadsheets that don't lend themselves well to larger teams or overlapping responsibilities.

IPAMs are useful tools in the network engineer's toolbox, allowing for a more complete modelling of IP space, physical (rack) space, a clear deliniation between VRFs and role-based access control. They're a friendlier mechanism for other teams to collaborate with you on IP management and allocations as well.

This short demo presents the tool; populates it with some data and updates data based on new changes.

## Create your environment


### To get an IPAM instance up and running:

```
git clone -b master https://github.com/netbox-community/netbox-docker.git
cd netbox-docker
docker-compose pull
docker-compose up -d
```

#### Determining the URL:

You can pick up the URL to copy and paste into your browser.

```
$ echo "http://$(docker-compose port nginx 8080)/"
http://0.0.0.0:32768/
```

Alternatively, open netbox in your default browser on macOS:
```
open "http://$(docker-compose port nginx 8080)/"
```
And open netbox in your default browser on (most) linuxes:
```
xdg-open "http://$(docker-compose port nginx 8080)/" &>/dev/null &
```

#### Default credentials:
```
Username: admin
Password: admin
API Token: 0123456789abcdef0123456789abcdef01234567
```

## Netbox usage & populating data
Netbox is an excellent application but some of its benefits aren't obvious without some data present.
There's a short script in the onug-ipam repository that can add some switches, place them in racks and assign some IP addresses to them.

### Install required packages
```
pip3 install --user termcolor pynetbox docker
```

### Clone the onug-ipam respository:
```
git clone git@gitlab.com:depereo/onug-ipam.git
cd onug-ipam/
```

### Create data (idempotent):
```
$ ~/onug-ipam/nb_populate.py
created site c99
created rack k01 in site c99
created rack f01 in site c99
created rack f16 in site c99
created manufacturer arista
created device type dcs-7050cx3-48yc8-r
created device type dcs-7060cx2-32s-r
created device role leaf
created device role spine
created device test-c99-f0101-sc01 in site c99
created device test-c99-f0102-ltr01a in site c99
created device test-c99-f0103-lyc01a in site c99
created device test-c99-f1602-ltr01b in site c99
created device test-c99-f1603-lyc01b in site c99
created device test-c99-k0101-sc01 in site c99
positioned device test-c99-f0101-sc01 in rack f01, u01
positioned device test-c99-f0102-ltr01a in rack f01, u02
positioned device test-c99-f0103-lyc01a in rack f01, u03
positioned device test-c99-f1602-ltr01b in rack f16, u02
positioned device test-c99-f1603-lyc01b in rack f16, u03
positioned device test-c99-k0101-sc01 in rack k01, u01
created ip 10.168.100.10/32
created ip 10.168.100.11/32
created ip 10.168.100.12/32
created ip 10.168.100.13/32
created ip 10.168.100.14/32
created ip 10.168.100.15/32
```

## Other options for imports

A quick way to migrate from a spreadsheet to this tool is a csv import. 
The included netbox_VLANS.csv file contents can be imported through the web GUI.

![](img/netbox_csv.png)

